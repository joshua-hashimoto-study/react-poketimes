import { configure } from '@storybook/react';
/**
 * config.jsにスタイルを当てるとサイトにも反映される
 */
import '../src/index.css'
import '../src/assets/materialize/css/materialize.min.css'

function loadStories() {
  require('../src/stories');
}

configure(loadStories, module);
