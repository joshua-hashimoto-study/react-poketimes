import React from "react";
import { Link } from "react-router-dom";
import pokeball from '../assets/img/pokeball.png'

const PostCell = props => {
    const { post } = props;

    return (
        <div className="post card">
            <img src={pokeball} alt=""/>
            <div className="card-content">
                <Link to={`/${post.id}`}>
                    <span className="card-title red-text">{post.title}</span>
                </Link>
                <p>{post.body}</p>
            </div>
        </div>
    );
};

export default PostCell;
