import React, { Component } from "react";
// import axios from "axios";
import PostCell from "./PostCell";
import { connect } from "react-redux";

// axiosを使った通信が関数ベースだと面倒だったのでクラスに変更
class Home extends Component {
    // state = {
    //     posts: []
    // };

    // componentDidMount() {
    //     axios
    //         .get("https://jsonplaceholder.typicode.com/posts")
    //         .then(res => {
    //             this.setState({
    //                 posts: res.data.slice(0, 10)
    //             });
    //         })
    // }

    render() {
        const { posts } = this.props;
        return (
            <div className="container home">
                <h4 className="center">Home</h4>
                {posts.length ? (
                    posts.map(post => {
                        return <PostCell post={post} key={post.id}/>;
                    })
                ) : (
                    <div className="center">no posts yet</div>
                )}
            </div>
        );
    }
}

// reduxからデータを受け取るためのメソッド
const mapStateToProps = (state) => {
    return {
        posts: state.posts
    }
}

// これをconnectで受け取る
export default connect(mapStateToProps)(Home);
