import React from 'react'
import Rainbow from '../hoc/Rainbow';

const About = () => {

    return (
        <div className="container">
            <h4 className="center">About</h4>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus voluptatem veritatis iusto omnis assumenda ipsa perferendis, sit consequuntur, saepe fugit ratione quam ducimus distinctio accusantium recusandae reiciendis voluptates porro! Sed.</p>
        </div>
    )
}

export default Rainbow(About)