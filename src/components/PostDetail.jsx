import React, { Component } from "react";
// import axios from 'axios'
import { connect } from "react-redux";
import { deletePost } from '../store/postActions';

// componentDidMountを使うためクラスベース
class PostDetail extends Component {
    // state = {
    //     post: null
    // }
    // componentDidMount() {
    //     let paramPostId = this.props.match.params.post_id
    //     axios.get(`https://jsonplaceholder.typicode.com/posts/${paramPostId}`).then(res => {
    //         this.setState({
    //             post: res.data
    //         })
    //     })
    // }

    handleClick = () => {
        const { post, deletePost } = this.props
        deletePost(post.id)
        this.props.history.push('/')
    }

    render() {
        const { post } = this.props;
        return (
            <div className="container">
                {post ? (
                    <div className="post">
                        <h4 className="center">{post.title}</h4>
                        <p>{post.body}</p>
                        <div className="center">
                            <button className="btn grey" onClick={this.handleClick}>
                                Delete Post
                            </button>
                        </div>
                    </div>
                ) : (
                    <div className="center">Loading post...</div>
                )}
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    let id = +ownProps.match.params.post_id;
    return {
        post: state.posts.find(post => post.id === id)
    };
};

const mapDispatchToProps = dispatch => {
    return {
        deletePost: id => {
            dispatch(deletePost(id));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PostDetail);
