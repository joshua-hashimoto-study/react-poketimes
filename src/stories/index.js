import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { linkTo } from "@storybook/addon-links";
import { Button, Welcome } from "@storybook/react/demo";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import NavBar from "../components/NavBar";
import Home from "../components/Home";
import PostCell from "../components/PostCell";
import PostDetail from "../components/PostDetail";
import About from "../components/About";
import Contact from '../components/Contact';

// storiesOf("Welcome", module).add("to Storybook", () => (
//     <Welcome showApp={linkTo("Button")} />
// ));

// storiesOf("Button", module)
//     .add("with text", () => (
//         <Button onClick={action("clicked")}>Hello Button</Button>
//     ))
//     .add("with some emoji", () => (
//         <Button onClick={action("clicked")}>
//             <span role="img" aria-label="so cool">
//                 😀 😎 👍 💯
//             </span>
//         </Button>
//     ));

storiesOf("Navigation", module).add("NavBar", () => (
    <BrowserRouter>
        <NavBar />
    </BrowserRouter>
));

storiesOf("Posts", module)
    .add("home", () => (
        <BrowserRouter>
            <Home />
        </BrowserRouter>
    ))
    .add("cell", () => (
        <BrowserRouter>
            <div className="container home">  {/* 細かいコンポーネントはその上についているcssも当てないといけない */}
                <PostCell
                    post={{ id: 1, title: "test title", body: "test body" }}
                />
            </div>
        </BrowserRouter>
    ))
    .add(
        "detail",
        () => (
            <BrowserRouter>
                <PostDetail
                    match={{ params: { post_id: 1 } }}
                    history={{ push: () => {} }}
                />
                {/* paramの指定法 */}
            </BrowserRouter>
        ),
        { query: { post_id: 1 } }
    );

storiesOf('About', module)
    .add('about', () => (
        <BrowserRouter>
            <About />
        </BrowserRouter>
    ))


storiesOf('Contact', module)
    .add('contanct', () => (
        <BrowserRouter>
            <Contact />
        </BrowserRouter>
    ))
