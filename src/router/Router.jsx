import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import NavBar from "../components/NavBar";
import Home from "../components/Home";
import About from "../components/About";
import Contact from "../components/Contact";
import PostDetail from "../components/PostDetail";


const Router = () => {
    return (
        <BrowserRouter>
            <NavBar />
            {/*  
                <Switch />はこの中の一つしかマッチして欲しくないということを保証する 
                しかしこの場合、動的に変わるurl(この場合はpath="/:post_id")は下に置かなければならない
            */}
            <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/about" component={About} />
                <Route path="/contact" component={Contact} />
                <Route path="/:post_id" component={PostDetail} />
            </Switch>
        </BrowserRouter>
    );
};

export default Router;
